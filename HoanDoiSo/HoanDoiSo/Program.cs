﻿using System;

namespace HoanDoiSo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            int a = 5;  
            int b = 10; 
            Console.WriteLine("Trước khi hoán đổi:");
            Console.WriteLine("a = " + a);
            Console.WriteLine("b = " + b);
            a = a + b;
            b = a - b;
            a = a - b;
            Console.WriteLine("Sau khi hoán đổi:");
            Console.WriteLine("a = " + a);
            Console.WriteLine("b = " + b);
        }
    }
}
